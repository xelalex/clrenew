<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {

            $table->increments('naic_cocode');
            $table->string('company_name',255);
            $table->string('mlg_address1',255);
            $table->string('mailing_city',255);
            $table->string('mailing_state',255);
            $table->string('mailing_zip',10);
            $table->integer('lob_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase');
        Schema::dropIfExists('purchases');
    }
}
